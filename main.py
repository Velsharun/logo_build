import os
import sys

from config import *
from api_mixin import ApiMixin
from file_mixin import FileMixin
from image_mixin import ImageMixin


class Script(FileMixin, ImageMixin):
    def __init__(self):
        api = ApiMixin(drive_document_id=drive_document_id)
        for row_num, row in enumerate(api.sheet):
            audio = False
            audio_book = False
            file = False
            if row_num == 0 or row[2]:
                continue
            args = api.get_data_by_id(row[0])
            if not args:
                row[5] = "Нет данных в апи\n"
                continue
            if args["type"] is not None or args["type2"] is not None:
                check_args = [args["type"], args["type2"]]
                if 1 in check_args:
                    audio = True
                if args["type"]:
                    audio_book = True
                if any(x in check_args for x in [0, 4]):
                    file = True
            file_id = row[1].split("/")[-1]
            templates = api.drive.ListFile({'q': "'%s' in parents and trashed=false" % file_id}).GetList()
            icons_dir = icons_path
            icons = os.listdir(icons_dir)
            for icon in icons:
                if icon.startswith("ch"):
                    file_icon = icon
                else:
                    audio_icon = icon
            if not templates:
                return
            file_obj = None
            crop_error = False
            for template in templates:
                template.GetContentFile(template_path+template["title"])
                file_obj = FileMixin(
                    template_image_name=template["title"],
                    author_name=args["author"],
                    book_title=args["title"],
                    rating=args["adult"],
                )
                cover_image = file_obj.get_image_by_url("https://"+args["picture"])
                image_obj = ImageMixin(
                    font_path_bold=font_path_bold,
                    font_path_regular=font_path_regular,
                    template_image_path=template_path+template["title"],
                    book_cover_path=cover_image,
                    author_mask=author_mask,
                    book_title_mask=book_title_mask,
                    rating_mask=rating_mask,
                    icons_mask=icons_mask,
                    author_text=args["author"],
                    book_title_text=args["title"],
                    rating_text=str(args["adult"]) + "+",
                    audio_icon=icons_dir+audio_icon,
                    file_icon=icons_dir+file_icon,
                    audio=audio,
                    file=file
                )
                image_obj.fill_canvas()
                try:
                    image_obj.write_text(
                        args["author"] if not author_caps else args["author"].upper(),
                        image_obj.font_path_bold,
                        image_obj.author_canvas,
                        color=author_font_color
                    )
                except Exception as e:
                    row[5] += f"""ERROR: {template["title"]} {e} Автор\n"""
                try:
                    image_obj.write_text(
                        args["title"] if not book_title_caps else args["title"].upper(),
                        image_obj.font_path_bold,
                        image_obj.book_title_canvas,
                        color=book_title_font_color
                    )
                except Exception as e:
                    row[5] += f"""ERROR: {template["title"]} {e} Название книги\n"""
                try:
                    image_obj.write_text(
                        str(args["adult"]) + "+",
                        image_obj.font_path_regular,
                        image_obj.rating_canvas,
                        color=rating_font_color,
                    )
                except Exception as e:
                    row[5] += f"""ERROR: {template["title"]} {e} Возраст\n"""
                crop = image_obj.fill_book_cover(audio_book)
                if crop and not crop_error:
                    row[5] += "WARNING: Обложку пришлось обрезать\n"
                    crop_error = True
                if audio or file:
                    image_obj.set_icon()
                file_obj.save_output_image_to_directory(image_obj.template_image)
            archive = file_obj.zip_files(file_obj.author_name+"-"+file_obj.book_title, lambda x: file_obj.latinize(file_obj.book_title) in x)
            # если архив создан - загружаем на диск
            if archive:
                drive_archive = api.drive.CreateFile({"title": archive.split("/")[-1]})
                drive_archive.SetContentFile(archive)
                drive_archive.Upload()
                new_permission = {
                    'id': 'anyoneWithLink',
                    'type': 'anyone',
                    'value': 'anyoneWithLink',
                    'withLink': True,
                    'role': 'reader'
                }
                permission = drive_archive.auth.service.permissions().insert(
                    fileId=drive_archive['id'], body=new_permission, supportsTeamDrives=True).execute(http=drive_archive.http)
                row[2] = drive_archive["alternateLink"]
                api.sheet.update_row(row_num+1, row)
                file_obj.clear_dir(otput_path)


def main():
    Script()


if __name__ == '__main__':
    main()
