# цвета, по которому определяем область, куда вставлять:
# текст с автором книги
author_mask = (255, 0, 227)
author_font_color = (0, 0, 0)
author_caps = True

# текст названия книги
book_title_mask = (0, 255, 233)
book_title_font_color = (0, 0, 0)
book_title_caps = True

# возрастной рейтинг снизу
rating_mask = (0, 0, 255)
rating_font_color = (255, 255, 255)

# иконки текста/аудио
icons_mask = (92, 223, 6)

# путь к шрифту по умолчанию
font_path_bold = "IBMPlexSans-Bold.ttf"
font_path_regular = "IBMPlexSans-Regular.ttf"

# каталог для шаблонов
template_path = "templates/"
# каталог для обложек
book_cover_path = "covers/"
# каталог для значков
icons_path = "icons/"
# каталог для готовых изображений
otput_path = "images"

# id документа с базой на диске
drive_document_id = "1XHfIAV3swYX0qjWIbnXFukLdZ0NNGDYtlDBDunmRlQA"
