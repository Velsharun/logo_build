from copy import copy
from math import atan, degrees
import textwrap

from PIL import Image, ImageDraw, ImageFont


class ImageMixin:
    """Класс для взаимодействия с файлами"""
    def __init__(self, *args, **kwargs):
        # шаблон информации по каждому полю
        canvas_template = {
            "min_x": None,
            "min_y": None,
            "x": 0,
            "y": 0,
            "gap_x": 0,
            "gap_y": 0,
            "rotate_angle": None,
        }

        icon_canvas = {
            "min_x": None,
            "min_y": None,
        }

        # Соотношения сторон для корректировки обложки
        self.mask_aspect_ratio = None
        self.book_cover_aspect_ratio = None

        self.author_canvas = copy(canvas_template)
        self.book_title_canvas = copy(canvas_template)
        self.rating_canvas = copy(canvas_template)
        self.book_cover_canvas = copy(canvas_template)

        self.audio_canvas = copy(icon_canvas)
        self.file_canvas = copy(icon_canvas)

        font_path_regular = kwargs.get("font_path_regular")
        if not font_path_regular:
            raise AttributeError("No regular font selected")
        self.font_path_regular = font_path_regular

        font_path_bold = kwargs.get("font_path_bold")
        if not font_path_bold:
            raise AttributeError("No bold font selected")
        self.font_path_bold = font_path_bold

        template_image = kwargs.get("template_image_path")
        if not template_image:
            raise AttributeError("No template image")
        self.template_image = Image.open(template_image)
        self.template_image = self.template_image.convert("RGBA")

        audio_icon = kwargs.get("audio_icon")
        if not audio_icon:
            raise AttributeError("No audio icon")
        self.audio_icon = Image.open(audio_icon)
        self.audio_icon = self.audio_icon.convert("RGBA")

        file_icon = kwargs.get("file_icon")
        if not file_icon:
            raise AttributeError("No file icon")
        self.file_icon = Image.open(file_icon)
        self.file_icon = self.file_icon.convert("RGBA")

        book_cover = kwargs.get("book_cover_path")
        if not book_cover:
            raise AttributeError("No book cover image")
        self.book_cover = Image.open(book_cover)
        self.book_cover = self.book_cover.convert("RGBA")

        self.author_color = kwargs.get("author_mask")
        self.book_title_color = kwargs.get("book_title_mask")
        self.rating_color = kwargs.get("rating_mask")
        self.icons_color = kwargs.get("icons_mask")

        self.audio = kwargs.get("audio")
        self.file = kwargs.get("file")

        self.icon_points_range = 0

    def write_text(self, text, font_path, canvas, color=(0, 0, 0), font_size=100):

        def rgb_to_hex(r, g, b):
            return ('{:02X}{:02X}{:02X}').format(r, g, b)

        def write_text_part(template_image, text, font_path, canvas, color=(0, 0, 0), font_size=100):
            if canvas["x"] == 0 or canvas["y"] == 0:
                raise Exception(f"Недостаточно точек для вставки текста. Проверьте цвет #{rgb_to_hex(*canvas['color'])}")
            width = 100
            jumpsize = 20
            count = 0
            for _ in range(0, 1000):
                font = ImageFont.truetype(font_path, font_size)
                text = textwrap.fill(text, width=width)
                w, h = font.getsize_multiline(text)
                ratio = round(canvas["x"] / canvas["y"], 1)
                text_ratio = round(w / h, 1)
                w_ratio = round(w / canvas["x"], 1)
                h_ratio = round(h / canvas["y"], 1)
                # ок по ширине
                if 0.8 <= w_ratio <= 1 and w <= canvas["x"]:
                    # ок по высоте
                    if 0.4 <= h_ratio <= 0.9:
                        # по размеру
                        if w <= canvas["x"] and h <= canvas["y"]:
                            break
                        # слишком высоко - пробуем растянуть текст без изменения шрифта
                        elif h > canvas["y"]:
                            jumpsize = jumpsize // 2
                            width += jumpsize if jumpsize else 1
                        # уменьшаем шрифт
                        elif w > canvas["x"]:
                            jumpsize = jumpsize // 2
                            font_size -= jumpsize if jumpsize else 1
                        elif text_ratio > ratio * 2:
                            width -= 1
                        else:
                            font_size += 1
                    elif text_ratio > ratio * 1.2:
                        width -= 1
                    else:
                        font_size -= 1
                elif h > canvas["y"] and ratio > text_ratio:
                    jumpsize = jumpsize // 2
                    width += jumpsize if jumpsize else 1
                    font_size -= 1
                # уменьшаем шрифт
                elif (w > canvas["x"] or h > canvas["y"]) and ratio <= text_ratio:
                    jumpsize = jumpsize // 2
                    font_size -= jumpsize if jumpsize else 1
                elif text_ratio > ratio * 1.2:
                    width -= 1
                else:
                    font_size += 1
                count += 1
            delta = (canvas["x"] - w) // 2
            ImageDraw.Draw(template_image).multiline_text((canvas["min_x"] + delta, canvas["min_y"]), text, font=font, fill=color)
            return w, h

        signs = [".", ",", "("]
        text_signs = [sign for sign in signs if sign in text]
        if text_signs:
            x, y = 0, 0
            # если есть точка - разбиваем текст на 2 части и запихиваем поочередно
            lines = [line for line in text.split(text_signs[0])]
            if text_signs[0] == "(":
                lines[-1] = "(" + lines[-1]
            if len(lines) > 2:
                new_lines = copy(lines)
                lines = [new_lines[0], "".join(new_lines[1:])]
            temp_canvas = copy(canvas)
            temp_canvas["y"] = canvas["y"] // 2
            for line in lines:
                temp_canvas["min_y"] += y
                x, y = write_text_part(self.template_image, line, font_path, temp_canvas, color=color, font_size=font_size)
        else:
            write_text_part(self.template_image, text, font_path, canvas, color=color, font_size=font_size)

    def fill_canvas(self):
        """
        Ищем по цветам области вставки текста и шаблона, заполняем следующие данные:
        Левый верхний угол контура, его ширина и высота
        FIXME пока без учета наклона
        """
        def fill_min(x, y, canvas, rewrite=False):
            if (not canvas.get("min_x") or canvas.get("min_x") > x) and \
                    (not canvas.get("min_y") or canvas.get("min_y") > y):
                canvas["min_x"] = x
                canvas["min_y"] = y
            if canvas["min_x"] and canvas["min_y"]:
                if canvas["min_x"] != x or canvas["min_y"] != y:
                    if x - canvas["min_x"] > canvas["x"]:
                        canvas["x"] = x - canvas["min_x"]
                    if y - canvas["min_y"] > canvas["y"]:
                        canvas["y"] = y - canvas["min_y"]
                if rewrite and y < canvas["min_y"]:
                    canvas["min_y"] = y

        prev_icon_x = 0
        for x in range(self.template_image.width):
            for y in range(self.template_image.height):
                repaint = False
                pixel_color = self.template_image.getpixel((x, y))
                if pixel_color[0:3] == self.author_color and pixel_color[-1] != 0:
                    fill_min(x, y, self.author_canvas)
                    self.author_canvas["color"] = pixel_color[0:3]
                    repaint = True
                if pixel_color[0:3] == self.book_title_color and pixel_color[-1] != 0:
                    fill_min(x, y, self.book_title_canvas)
                    self.book_title_canvas["color"] = pixel_color[0:3]
                    repaint = True
                if pixel_color[0:3] == self.rating_color and pixel_color[-1] != 0:
                    fill_min(x, y, self.rating_canvas)
                    self.rating_canvas["color"] = pixel_color[0:3]
                    repaint = True
                if pixel_color[0:3] == self.icons_color and pixel_color[-1] != 0:
                    if prev_icon_x > 0:
                        self.icon_points_range = max(self.icon_points_range, x - prev_icon_x)
                    prev_icon_x = x
                    if self.audio is not None and all([value is None for value in self.audio_canvas.values()]):
                        self.audio_canvas["min_x"] = x
                        self.audio_canvas["min_y"] = y
                    elif self.file is not None and all([value is None for value in self.file_canvas.values()]):
                        self.file_canvas["min_x"] = x
                        self.file_canvas["min_y"] = y
                    repaint = True
                if pixel_color[-1] == 0:
                    fill_min(x, y, self.book_cover_canvas, rewrite=True)
                    self.book_cover_canvas["color"] = pixel_color
                if repaint:
                    if x > 0:
                        if self.template_image.getpixel((x-1, y)) != pixel_color:
                            self.template_image.putpixel((x, y), self.template_image.getpixel((x-1, y)))
                        elif self.template_image.getpixel((x, y-1)) != pixel_color:
                            self.template_image.putpixel((x, y), self.template_image.getpixel((x, y-1)))

    def fill_book_cover(self, audio_book=False):
        frame = self.book_cover_canvas
        crop_flag = False
        w, h = self.book_cover.size
        if audio_book and w != h:
            min_size = min(w, h)
            self.book_cover = self.book_cover.crop((0, 0, min_size, min_size))
            crop_flag = True
        while True:
            w, h = self.book_cover.size
            if frame["x"] != w:
                frame["aspect_ratio"] = w / h
                if not crop_flag:
                    self.book_cover = self.book_cover.resize((frame["x"], round(frame["x"] / frame["aspect_ratio"])))
                else:
                    self.book_cover = self.book_cover.resize((frame["x"], frame["x"]))
                if crop_flag:
                    break
            elif frame["y"] < h:
                self.book_cover = self.book_cover.crop((0, 0, frame["x"], frame["y"]))
            else:
                break
        book_cover_bg = Image.new("RGBA", self.template_image.size)
        book_cover_bg.paste(self.book_cover, box=(frame["min_x"], frame["min_y"]))
        book_cover_bg.alpha_composite(self.template_image)
        self.template_image = book_cover_bg
        return crop_flag

    def set_icon(self):
        def paste_icon(canvas, icon):
            coeff = self.icon_points_range * 0.7
            icon = icon.resize((round(coeff), round(coeff)), resample=Image.LANCZOS)
            top_x = icon.size[0]
            top_y = icon.size[1]
            icon_bg = Image.new("RGBA", self.template_image.size)
            icon_bg.paste(icon, box=(canvas["min_x"] - top_x // 2, canvas["min_y"] - top_y // 2))
            self.template_image.alpha_composite(icon_bg)
        if self.audio:
            paste_icon(self.audio_canvas, self.audio_icon)
        if self.file:
            paste_icon(self.file_canvas, self.file_icon)


