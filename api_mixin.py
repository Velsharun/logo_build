import requests
from requests.exceptions import JSONDecodeError

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import pygsheets


class ApiMixin:
    """Класс для взаимодействия с API Google"""
    def __init__(self, *args, **kwargs):
        gauth = GoogleAuth()
        gauth.LocalWebserverAuth()
        self.drive = GoogleDrive(gauth)
        gc = pygsheets.authorize(service_file="client_secret.json")
        drive_document_id = kwargs.get("drive_document_id")
        if not drive_document_id:
            raise AttributeError("No main document")
        table = gc.open_by_key(drive_document_id)
        self.sheet = table[0]

    def get_data_by_id(self, id):
        """ Берет по айди данные об авторе, название, рейтинг со стикерами и путь к обложке"""
        try:
            return requests.get(f"ji/{id}").json()
        except JSONDecodeError:
            return {}
