from zipfile import ZipFile
import os
from os.path import basename
from urllib import request
import io

dictionary = {
    "а": "a",
    "б": "b",
    "в": "v",
    "г": "g",
    "д": "d",
    "е": "e",
    "ё": "yo",
    "ж": "zh",
    "з": "z",
    "и": "i",
    "й": "y",
    "к": "k",
    "л": "l",
    "м": "m",
    "н": "n",
    "о": "o",
    "п": "p",
    "р": "r",
    "с": "s",
    "т": "t",
    "у": "u",
    "ф": "f",
    "х": "h",
    "ц": "ts",
    "ч": "ch",
    "ш": "sh",
    "щ": "shch",
    "ъ": "y",
    "ы": "y",
    "ь": "",
    "э": "e",
    "ю": "yu",
    "я": "ya",
    "А": "A",
    "Б": "B",
    "В": "V",
    "Г": "G",
    "Д": "D",
    "Е": "E",
    "Ё": "YO",
    "Ж": "ZH",
    "З": "Z",
    "И": "I",
    "Й": "Y",
    "К": "K",
    "Л": "L",
    "М": "M",
    "Н": "N",
    "О": "O",
    "П": "P",
    "Р": "R",
    "С": "S",
    "Т": "T",
    "У": "U",
    "Ф": "F",
    "Х": "H",
    "Ц": "TS",
    "Ч": "CH",
    "Ш": "SH",
    "Щ": "SHCH",
    "Ъ": "Y",
    "Ы": "Y",
    "Ь": "",
    "Э": "E",
    "Ю": "YU",
    "Я": "YA",
}


class FileMixin:
    """Класс для взаимодействия с файлами"""
    def __init__(self, *args, **kwargs):
        self.template_image_path = None
        self.book_cover_path = None
        self.output_image_name = ""
        self.output_image_folder = ""

        template_image_name = kwargs.get("template_image_name")
        if not template_image_name:
            raise AttributeError("No template image")
        self.template_image_name = template_image_name

        author_name = kwargs.get("author_name")
        if not author_name:
            raise AttributeError("No author name")
        self.author_name = author_name

        book_title = kwargs.get("book_title")
        if not book_title:
            raise AttributeError("No book title")
        self.book_title = book_title

        rating = kwargs.get("rating")
        if not book_title:
            raise AttributeError("No book rating")
        self.rating = rating

        curr_directory = os.path.dirname(os.path.abspath(__file__))
        self.output_image_folder = curr_directory + "/images/" + self.author_name + " " + self.book_title + "/"

    def latinize(self, image_name):
        result = ""
        for letter in image_name:
            if letter in dictionary.keys():
                result += dictionary[letter]
            else:
                result += letter
        return result

    def save_output_image_to_directory(self, template_image):
        if not os.path.exists(self.output_image_folder):
            os.makedirs(self.output_image_folder)
        self.output_image_name = self.template_image_name.split(".")[0] + \
                                 self.latinize(" " + self.author_name + "-" + self.book_title) + ".png"
        image_file_name = self.output_image_folder + self.output_image_name
        template_image.save(image_file_name)

    def get_image_by_url(self, url):
        with request.urlopen(url) as web_image:
            image_bytes = io.BytesIO(web_image.read())
        return image_bytes

    def clear_dir(self, path):
        for root, dirs, files in os.walk(path, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))

    def zip_files(self, archive_name, name_filter):
        file_archive = None
        with ZipFile(self.output_image_folder+archive_name+".zip", "w") as archive:
            for folder_name, subfolders, filenames in os.walk(self.output_image_folder):
                for filename in filenames:
                    if name_filter(filename):
                        file_path = os.path.join(folder_name, filename)
                        archive.write(file_path, basename(file_path))
                        os.remove(file_path)
            file_archive = archive.filename
        return file_archive
